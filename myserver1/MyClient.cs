using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcClient;


namespace myserver1
{
    public class MyClient
    {
        private readonly GrpcChannel _channel;

        public MyClient(string serverUrl)
        {
            _channel = GrpcChannel.ForAddress(serverUrl);
        }

        public async Task CallOtherServer()
        {
            // create client and pass the channel
            var client = new GrpcClient.Weather.WeatherClient(_channel);

            // call the method from the client
            var weatherInfo = await client.GetUpdateAsync(new Location
            {
                Latitude = 14.10,
                Longitude = 77.00
            });

            foreach (PropertyInfo prop in weatherInfo.GetType().GetProperties())
            {
                var name = prop.Name;
                var value = prop.GetValue(weatherInfo, null);
                Console.WriteLine($"{name}=>{value}");
            }

        }
    }

}