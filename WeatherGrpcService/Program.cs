using WeatherGrpcService.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddGrpc();
var app = builder.Build();

// Configure the HTTP request pipeline.
// Configure method
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapGrpcService<WeatherService>();
    endpoints.MapGrpcService<PingService>();
    endpoints.MapGrpcService<DittoService>();

});
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();