using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace WeatherGrpcService.Services
{
    public class PingService : WeatherGrpcService.Ping.PingBase
    {
        //responseStream: This is the output stream on which the server can write as much as 
        //it likes to send data to the client over the time
        public override async Task DoRepeatReply(Message request
        , IServerStreamWriter<Message> responseStream
        , ServerCallContext context)
        {
            Console.WriteLine($"Initial Message from Client: {request.Msg}");

            try
            {
                while (!context.CancellationToken.IsCancellationRequested)
                {
                    Thread.Sleep(10000);
                    await responseStream.WriteAsync(new Message
                    {
                        Msg = $"Ping Response from the Server at {DateTime.UtcNow}"
                    });
                }
            }
            catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
            {
                Console.WriteLine("Operation Cancelled.");
            }

            Console.WriteLine("Processing Complete.");
        }
    }
}