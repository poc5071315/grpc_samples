using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace WeatherGrpcService.Services
{
    public class DittoService : WeatherGrpcService.Ditto.DittoBase
    {
        public override async Task Speak(IAsyncStreamReader<Msg> requestStream, IServerStreamWriter<Msg> responseStream, ServerCallContext context)
        {
            try
            {
                while (await requestStream.MoveNext()
                  && !context.CancellationToken.IsCancellationRequested)
                {
                    // read incoming message 
                    var current = requestStream.Current;
                    Console.WriteLine($"Message from Client: {current.Text}");

                    // write outgoing message
                    await SendResponseMessage(current, responseStream);
                }
            }
            catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
            {
                Console.WriteLine("Operation Cancelled");
            }

            Console.WriteLine("Operation Complete.");
        }

        private async Task SendResponseMessage(Msg current,
            IServerStreamWriter<Msg> responseStream)
        {
            await responseStream.WriteAsync(new Msg
            {
                Text = $"Ditto from Server: {current.Text}"
            });
        }
    }
}