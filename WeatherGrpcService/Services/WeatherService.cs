using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using WeatherGrpcService;

namespace WeatherGrpcService.Services
{
    public class WeatherService : Weather.WeatherBase
    {
        public override Task<WeatherInfo> GetUpdate(Location request, ServerCallContext context)
        {
            return Task.FromResult(new WeatherInfo
            {
                Humidity = 34.0,
                Precipitation = 4,
                Temperature = 34,
                IsRainfallExpected = false,
                Prediction = $"Weather Update for {request.Latitude}, {request.Longitude} => Its going to be a sunny day."
            });
        }
    }
}