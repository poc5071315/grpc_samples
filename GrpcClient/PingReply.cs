using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace GrpcClient;
    public class PingReply
{
        public static async Task CallPingReply(Ping.PingClient client)
        {
            // some random request from client
            var request = new Message { Msg = "Hello Ping!" };

            // a cancellationToken to cancel the operation after sometime
            // in this case the operation (reading from the server) is cancelled
            // after 60 seconds from the invocation
            var cancellationToken = new CancellationTokenSource(
              TimeSpan.FromSeconds(60));

            try
            {
                // call the server method which returns a stream 
                // pass the cancellationToken along side so that the operation gets
                // cancelled when needed
                AsyncServerStreamingCall<Message> response =
                    client.DoRepeatReply(request,
                      cancellationToken: cancellationToken.Token);

                // loop through each object from the ResponseStream
                while (await response.ResponseStream.MoveNext())
                {
                    // fetch the object currently pointed
                    var current = response.ResponseStream.Current;

                    // print it
                    Console.WriteLine($"{current.Msg}");
                }
            }
            catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
            {
                Console.WriteLine("Operation Cancelled.");
            }

            Console.ReadLine();
        } 
}