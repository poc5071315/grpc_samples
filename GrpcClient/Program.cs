﻿using System.Reflection;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcClient;
using nugetPackageDemo.GreetingLibrary;



// create channel
//https://localhost:7156
var channel = GrpcChannel.ForAddress("https://localhost:7172");

/*
// create client and pass the channel
var client = new GrpcClient.Weather.WeatherClient(channel);

// call the method from the client
var weatherInfo = await client.GetUpdateAsync(new Location
{
    Latitude = 14.10,
    Longitude = 77.00
});

foreach (PropertyInfo prop in weatherInfo.GetType().GetProperties())
{
    var name = prop.Name;
    var value = prop.GetValue(weatherInfo, null);
    Console.WriteLine($"{name}=>{ value}"); 
}
//Console.ReadLine();

// call the PingService
await PingReply.CallPingReply(new Ping.PingClient(channel));

// call the DittoService
await DittoReply.CallDitto(new Ditto.DittoClient(channel));

//Greeter greet = new Greeter();
*/

var client2 = new GrpcClient.Greeter.GreeterClient(channel);
var response = await client2.SayHelloAsync(new HelloRequest(){Name="Ahmed"});
System.Console.WriteLine(response);