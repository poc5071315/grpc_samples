using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace GrpcClient
{
    public class DittoReply
    {
        public static async Task CallDitto(Ditto.DittoClient client)
        {
            var source = new CancellationTokenSource();

            try
            {
                using AsyncDuplexStreamingCall<Msg, Msg> stream =
                    client.Speak(cancellationToken: source.Token);

                Console.WriteLine(
                  "Type something and press Enter. Enter Q to Quit.");

                while (true)
                {
                    var input = Console.ReadLine();

                    if (input.ToLower() == "q")
                    {
                        await stream.RequestStream.CompleteAsync();
                        break;
                    }

                    // write to stream
                    await WriteToStream(stream.RequestStream, input);

                    // Read from stream
                    ReadFromStream(stream.ResponseStream);
                }

                Console.WriteLine("Client Complete.");
            }
            catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
            {
                Console.WriteLine("Operation Cancelled");
            }
        }

        private static async Task ReadFromStream(
          IAsyncStreamReader<Msg> responseStream)
        {
            while (await responseStream.MoveNext())
            {
                Console.WriteLine(responseStream.Current.Text);
            }
        }

        private static async Task WriteToStream(
          IClientStreamWriter<Msg> requestStream, string input)
        {
            await requestStream.WriteAsync(new Msg { Text = input });
        }
    }
}